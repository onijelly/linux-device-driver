/*
 * Phi Nguyen <phind.uet@gmail.com>
 * Task 09 of the Eudyptula challenge.
 * - Take the code you wrote in task 08, and move it to sysfs.  Put the
 *   "eudyptula" directory under the /sys/kernel/ location in sysfs.
 * Function to use: 
 *  kobject_create_and_add(),
 *
 */
#include <linux/module.h>
#include <linux/init.h>
#include <linux/kobject.h>

#define MODULE_NAME         "eudyptula"
#define MY_ID               "11020423"

static char foo_buffer[PAGE_SIZE];
static struct kobject *eud_kobject;

/*
 * The "id" file where a we can read the id
 */
static ssize_t id_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
  return snprintf(buf, sizeof(MY_ID), "%s\n", (char*)MY_ID);
}

static ssize_t id_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
  int cmp_size = count > sizeof(MY_ID) ? sizeof(MY_ID) : count - 1; //Do not take the \n
  if (strncmp(buf, (char*)MY_ID, cmp_size) == 0) {
    pr_info("eudyptula ID match\n");
    return count;
  } else {
    pr_err("eudyptula ID is not match\n");
    return -EINVAL;
  }
}

static struct kobj_attribute id_attribute = 
  __ATTR(id, 0664 , id_show, id_store);

/* The "jiffies" file for reading current jiffy */
static ssize_t jiffies_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
  u64 jif = get_jiffies_64();
  return snprintf(buf, 20, "%llu\n", jif);
}

static struct kobj_attribute jiffies_attribute = 
  __ATTR(jiffies, 0444, jiffies_show, NULL);

/* The "foo" file for reading and writing some dummy stuff*/
static ssize_t foo_show(struct kobject *kobj, struct kobj_attribute *attr, char *buf)
{
  return sprintf(buf, "%s", foo_buffer);
}

static ssize_t foo_store(struct kobject *kobj, struct kobj_attribute *attr, const char *buf, size_t count)
{
  memset(foo_buffer, 0, PAGE_SIZE);
  return snprintf(foo_buffer, count > PAGE_SIZE ? PAGE_SIZE : count, "%s", buf);
}

static struct kobj_attribute foo_attribute = 
  __ATTR(foo, 0644, foo_show, foo_store);

/* Create a group of attributes so that we can create and destroy them all at once */
static struct attribute *attrs[] = {
    &id_attribute.attr,
    &jiffies_attribute.attr,
    &foo_attribute.attr,
    NULL,
};

static struct attribute_group attr_group = {
  .attrs = attrs,
};

int __init eudyptula_init(void)
{
  int ret = 0;

  /* This call will create a directory in /sys/kernel */
  eud_kobject = kobject_create_and_add(MODULE_NAME, kernel_kobj);

  if (IS_ERR(eud_kobject)) {
    ret = PTR_ERR(eud_kobject);
    pr_err("Could not create kobject\n");
    goto out;
  }
  ret = sysfs_create_group(eud_kobject, &attr_group);
  if (ret)
    kobject_put(eud_kobject);
out:
  return ret;
}

void __exit eudyptula_exit(void)
{
  kobject_put(eud_kobject);
  pr_info("Removed Eudyptula module\n");
}

module_init(eudyptula_init);
module_exit(eudyptula_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Phi Nguyen<phind.uet@gmail.com>");
MODULE_DESCRIPTION("Simplest kernel module");
