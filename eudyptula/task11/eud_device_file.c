/*
 * Phi Nguyen <phind.uet@gmail.com>
 * Task 11 of Eudyptula challenge.
 * Create a "id" file under /sys/devices folder.
 * This file will be read and write by any user,
 */
#include <linux/module.h>
#include <linux/init.h>
#include <linux/device.h>
#include <linux/miscdevice.h>

#define MODULE_NAME "eudyptula"

static ssize_t eud_show(struct device* dev, struct device_attribute *attr, char* buf) 
{
  return snprintf(buf, sizeof(MODULE_NAME), "%s\n", MODULE_NAME);
}

static ssize_t eud_store(struct device* dev, struct device_attribute *attr, const char* buf, size_t count) 
{
  if (strncmp((char*)MODULE_NAME, buf, count > sizeof(MODULE_NAME) ? sizeof(MODULE_NAME) : count - 1 )) 
    return -EINVAL;
  return count;
}

static DEVICE_ATTR(id, 0664, eud_show, eud_store);

static struct miscdevice eud_misc = {
  .name = MODULE_NAME,
  .minor = MISC_DYNAMIC_MINOR,
};

int __init eud_init(void)
{
  int ret;

  ret = misc_register(&eud_misc);
  if (ret) {
    pr_err("Could not register the miscdevice\n");
    return ret;
  }

  ret = device_create_file(eud_misc.this_device, &dev_attr_id);
  if (ret) {
    pr_err("cannot create device file");
    misc_deregister(&eud_misc);
    return ret;
  }

  pr_info("eudyptula device created!\n");
  return 0;
}

void __exit eud_exit(void)
{
  device_remove_file(eud_misc.this_device, &dev_attr_id);
  misc_deregister(&eud_misc);
  pr_info("eudyptula end!\n");
}

module_init(eud_init);
module_exit(eud_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Phi Nguyen<phind.uet@gmail.com>");
MODULE_DESCRIPTION("Simplest kernel module");
