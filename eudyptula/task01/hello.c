/*
 * Phi Nguyen <phind.uet@gmail.com>
 * This is source code for a simpile kernel module to demonstrate the loading
 * and unloading operation
 */
#include <linux/module.h>
#include <linux/init.h>

int __init hello_init(void)
{
  pr_info("Hello world!\n");
  return 0;
}

void __exit hello_exit(void)
{
  pr_info("Goodbye world!\n");
}

module_init(hello_init);
module_exit(hello_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Phi Nguyen<phind.uet@gmail.com>");
MODULE_DESCRIPTION("Simplest kernel module");
