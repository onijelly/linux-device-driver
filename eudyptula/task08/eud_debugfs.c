/*
 * Phi Nguyen <phind.uet@gmail.com>
 * Task 08
 * This is source code for a simpile kernel module to demonstrate the debugfs
 * - Take the kernel module you wrote for task 01, and modify it to be
 *   create a debugfs subdirectory called "eudyptula".  In that
 *   directory, create 3 virtual files called "id", "jiffies", and "foo".
 * 1 The file "id" operates just like it did for example 06, use the same
 *   logic there, the file must be readable and writable by any user.
 * 2 The file "jiffies" is to be read only by any user, and when read,
 *   should return the current value of the jiffies kernel timer.
 * 3 The file "foo" needs to be writable only by root, but readable by
 *   anyone.  When writing to it, the value must be stored, up to one
 *   page of data.  When read, which can be done by any user, the value
 *   must be returned that is stored it it.  Properly handle the fact
 *   that someone could be reading from the file while someone else is
 *   writing to it (oh, a locking hint!)
 * - When the module is unloaded, all of the debugfs files are cleaned
 *   up, and any memory allocated is freed.
 */

#include <linux/module.h>
#include <linux/device.h>
#include <linux/init.h>
#include <linux/debugfs.h>
#include <linux/miscdevice.h>
#include <linux/uaccess.h>
#include <linux/spinlock.h>

#define MY_ID "11020423"
#define DEBUGFS_DIR "eudyptula"
#define DEBUGFS_ID "id"
#define DEBUGFS_TIME "jiffies"
#define DEBUGFS_FOO "foo"

static DEFINE_RWLOCK(foo_rwlock);

enum file_idx {
  ID_FILE,
  JIFFIES_FILE,
  FOO_FILE
};

/* Dentry means debug entry */
struct mydebug_entry {
  struct dentry* dentry;
  char* buffer;
  enum file_idx idx;
  ssize_t buffer_size;
};

static struct dentry* top_dir;

static struct miscdevice eudyptula_misc = {
  .minor = MISC_DYNAMIC_MINOR,
  .name = DEBUGFS_DIR,
};

int debugfs_open(struct inode* i, struct file* f)
{
  struct mydebug_entry* data = i->i_private;
  f->private_data = data;

  if (data->idx == JIFFIES_FILE)
  {
    char* jif_buff = devm_kzalloc(eudyptula_misc.this_device, 20 , GFP_KERNEL);
    snprintf(jif_buff, 20 , "%llu", get_jiffies_64());
    data->buffer = jif_buff;
    data->buffer_size = 20;
  }

  return 0;
}

static ssize_t debugfs_read(struct file* f, char __user *buf, size_t len, loff_t* pos)
{
  ssize_t to_read;
  struct mydebug_entry* data = f->private_data;

  if (data->idx == FOO_FILE){
    read_lock(&foo_rwlock);
  }

  to_read = data->buffer_size - *pos > len ? len : data->buffer_size - *pos;
  if (to_read < 0)
    return 0;

  if (copy_to_user(buf, data->buffer + *pos, to_read))
    return -EFAULT;

  *pos += to_read;

  if (data->idx == FOO_FILE){
    read_unlock(&foo_rwlock);
  }

  return to_read;
}

static ssize_t debugfs_write(struct file* f, const char __user *buf, size_t len, loff_t* pos)
{
  ssize_t to_write;
  char *temp;
  struct mydebug_entry *data = f->private_data;
  switch (data->idx){
    case FOO_FILE:
      write_lock(&foo_rwlock);
      to_write = data->buffer_size - *pos > len ? len : data->buffer_size - *pos;
      if (to_write < 0) {
        return 0;
      }

      if (copy_from_user(data->buffer + *pos, buf, to_write))
        return -EFAULT;
      *pos += to_write;
      write_unlock(&foo_rwlock);
      break;
    case ID_FILE:
      temp = devm_kmalloc(eudyptula_misc.this_device, sizeof(MY_ID), GFP_KERNEL);
      to_write = data->buffer_size - *pos > len ? len : data->buffer_size - *pos;
      if (to_write < 0) {
        return 0;
      }
      if (copy_from_user(temp + *pos, buf, to_write))
        return -EFAULT;
      *pos += to_write;
      if (strncmp(temp, data->buffer, to_write - 1) != 0)
      {
        pr_err("The read string %s(len: %ld) does not match %s\n", temp, to_write, data->buffer);
        to_write = -EINVAL; 
      }
      break;
    default:
      return -EINVAL;
  }
  return to_write;
}

static struct file_operations debug_fops = {
  .owner = THIS_MODULE,
  .open = debugfs_open,
  .read = debugfs_read,
  .write = debugfs_write,
};

int __init hello_init(void)
{
  int ret;
  struct mydebug_entry *id, *jiffies, *foo;

  // I will use a misc device for using devm allocation
  ret = misc_register(&eudyptula_misc);
  if (ret != 0){
    pr_err("Canot register miscdevice \n");
    return ret; 
  }

  /* Allocate debug entries */
  if ((id = devm_kmalloc(eudyptula_misc.this_device, sizeof(struct mydebug_entry), GFP_KERNEL)) == NULL) {
    ret = -ENOMEM;
    goto memory_failed;
  }

  if ((jiffies = devm_kmalloc(eudyptula_misc.this_device, sizeof(struct mydebug_entry), GFP_KERNEL)) == NULL) {
    ret = -ENOMEM;
    goto memory_failed;
  }

  if ((foo = devm_kmalloc(eudyptula_misc.this_device, sizeof(struct mydebug_entry), GFP_KERNEL)) == NULL) {
    ret = -ENOMEM;
    goto memory_failed;
  }

  top_dir = debugfs_create_dir(DEBUGFS_DIR, NULL);
  if (IS_ERR(top_dir)) {
    pr_err("Could not create debugfs directory\n");
    ret = PTR_ERR(top_dir);
    goto out;
  }
  
  id->idx = ID_FILE;
  id->buffer = (char*)MY_ID;
  id->buffer_size = sizeof(MY_ID); // I think it is in stack.
  id->dentry = debugfs_create_file(DEBUGFS_ID, S_IRUGO|S_IWUGO, top_dir, id, &debug_fops);
  if (IS_ERR(id->dentry)) {
    pr_err("Could not create id file\n");
    ret = PTR_ERR(id->dentry);
    goto create_file_failed;
  }

  jiffies->idx = JIFFIES_FILE;
  jiffies->dentry = debugfs_create_file(DEBUGFS_TIME, S_IRUGO, top_dir, jiffies, &debug_fops);
  if (IS_ERR(jiffies->dentry)){
    pr_err("Could not create jiffies file\n");
    ret = PTR_ERR(jiffies->dentry);
    goto create_file_failed;
  }

  foo->idx = FOO_FILE;
  foo->buffer = devm_kzalloc(eudyptula_misc.this_device, PAGE_SIZE, GFP_KERNEL);
  if (foo->buffer == NULL)
  {
    ret = -ENOMEM;
    goto memory_failed;
  }
  foo->buffer_size = PAGE_SIZE;
  foo->dentry = debugfs_create_file(DEBUGFS_FOO, S_IRUGO| S_IWUSR, top_dir, foo, &debug_fops);
  if (IS_ERR(foo->dentry)) {
    pr_err("Could not create foo file\n");
    ret = PTR_ERR(foo->dentry);
    goto create_file_failed;
  }

  pr_info("Module inserted successfully \n");
  
out:
  return 0;
create_file_failed:
  debugfs_remove_recursive(top_dir);
memory_failed:
  misc_deregister(&eudyptula_misc);
  return ret;
}

void __exit hello_exit(void)
{
  debugfs_remove_recursive(top_dir);
  misc_deregister(&eudyptula_misc);
  pr_info("Goodbye world!\n");
}

module_init(hello_init);
module_exit(hello_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Phi Nguyen<phind.uet@gmail.com>");
MODULE_DESCRIPTION("Simplest kernel module");
