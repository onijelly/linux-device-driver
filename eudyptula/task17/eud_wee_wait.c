/*
 * Phi Nguyen <phind.uet@gmail.com>
 * This is source code for a simpile kernel module to demonstrate the loading
 * and unloading operation
 */
#include <linux/module.h>
#include <linux/init.h>
#include <linux/miscdevice.h>
#include <linux/uaccess.h>
#include <linux/fs.h>
#include <linux/wait.h>
#include <linux/kthread.h>

#define MODULE_NAME "eudyptula"
DECLARE_WAIT_QUEUE_HEAD(wee_wait);
static struct task_struct *eudyptula_thread;

static ssize_t misc_write(struct file* f, const char __user *buf, size_t buf_size, loff_t* pos)
{
  char kbuf[256];
  int ret;

  if (*pos + buf_size > 255) {
    ret = -EFAULT;
    goto out;
  }

  ret = copy_from_user(kbuf, buf, buf_size);
out:
  return ret;
}

static struct file_operations misc_fops = {
 .write = misc_write,
 .owner = THIS_MODULE,
};

static struct miscdevice eudyptula_misc = {
  .name = MODULE_NAME,
  .minor = MISC_DYNAMIC_MINOR,
  .fops = &misc_fops,
  .mode = S_IWUSR | S_IWGRP | S_IWOTH,
};

int eudyptula_fn(void *data) {
  pr_info("Enter kthread \n");
  do {
    set_current_state(TASK_INTERRUPTIBLE);
    wait_event_interruptible(wee_wait, kthread_should_stop());
    set_current_state(TASK_RUNNING);
  } while (!kthread_should_stop());
  pr_info("Return from kthread\n");
  return 0;
}

int __init hello_init(void)
{
  int ret;

  pr_info("Hello world!\n");
  ret = misc_register(&eudyptula_misc);
  
  if (ret < 0) {
    pr_err("Misc registration was failed\n");
    return -ENODEV;
  }

  eudyptula_thread = kthread_create(eudyptula_fn, NULL, "eudyptula");
  if (IS_ERR(eudyptula_thread)) {
    misc_deregister(&eudyptula_misc);
    return PTR_ERR(eudyptula_thread);
  }
  wake_up_process(eudyptula_thread);
  
  return 0;
}

void __exit hello_exit(void)
{
  kthread_stop(eudyptula_thread);
  misc_deregister(&eudyptula_misc);
  pr_info("Goodbye world!\n");
}

module_init(hello_init);
module_exit(hello_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Phi Nguyen<phind.uet@gmail.com>");
MODULE_DESCRIPTION("Simplest kernel module");
