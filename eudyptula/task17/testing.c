#include <stdio.h>
#include <sys/types.h>
#include <unistd.h>
#include <sys/stat.h>
#include <fcntl.h>

int main()
{
  int fd;
  char buff[255];
  mode_t mode = S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH;
  fd = open("/dev/eudyptula", O_RDONLY, mode);
  read(fd, buff, 8);
  printf("%s\n", buff);

  close(fd);
  return 0;
}
