/*
 * Phi Nguyen <phind.uet@gmail.com>
 * This is source code for a simpile kernel module to demonstrate the loading
 * and unloading operation
 */
#include <linux/module.h>
#include <linux/init.h>
#include <linux/usb.h>
#include <linux/hid.h>

#define DRIVER_NAME "task05-usb"

static struct usb_device_id usb_ids[] = {
  {USB_INTERFACE_INFO(
      USB_INTERFACE_CLASS_HID,
      USB_INTERFACE_SUBCLASS_BOOT,
      USB_INTERFACE_PROTOCOL_KEYBOARD
  )},
  {}
};

static int task05_probe(struct usb_interface *inf, const struct usb_device_id *id)
{
  pr_info("Task05 USB driver probed\n");
  return 0;
}

static void task05_disc(struct usb_interface *intf)
{
  pr_info("Task05 USB driver disconnected\n");
}

static struct usb_driver task05 = {
 .name = DRIVER_NAME,
 .id_table = usb_ids,
 .probe = task05_probe,
 .disconnect = task05_disc,
};

int __init hello_init(void)
{
  int ret;
  pr_info("Hello world!\n");
  ret = usb_register(&task05);
  if (ret < 0) {
    pr_err("usb_register failed\n");
    return -1;
  }
  return 0;
}

void __exit hello_exit(void)
{
  usb_deregister(&task05);
  pr_info("Goodbye world!\n");
}

module_init(hello_init);
module_exit(hello_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Phi Nguyen<phind.uet@gmail.com>");
MODULE_DESCRIPTION("Simplest kernel module");
