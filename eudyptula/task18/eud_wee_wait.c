/*
 * Phi Nguyen <phind.uet@gmail.com>
 * This is source code for a simpile kernel module to demonstrate the loading
 * and unloading operation
 */
#include <linux/module.h>
#include <linux/miscdevice.h>
#include <linux/poll.h>
#include <linux/errno.h>
#include <linux/wait.h>
#include <linux/kthread.h>
#include <linux/sched.h>
#include <linux/slab.h>
#include <linux/delay.h>
#include <linux/mutex.h>

#define MODULE_NAME "eudyptula"
#define NAME_LEN 19
struct identity 
{
  char name[20];
  int id;
  bool busy;
  struct list_head list_node; 
};

DECLARE_WAIT_QUEUE_HEAD(wee_wait);
static struct task_struct *eudyptula_thread;
static struct kmem_cache *eud_poll;
LIST_HEAD(identity_head);
static DEFINE_MUTEX(i_mutex);
static int counter;

static int identity_create(char* name, int id)
{
  struct identity *new_item = kmem_cache_alloc(eud_poll, GFP_KERNEL);
  if (IS_ERR(new_item))
    return PTR_ERR(new_item);
  new_item->id = id;
  memcpy(new_item->name, name, 20 > strlen(name) ? strlen(name) : 20);
  list_add(&new_item->list_node, &identity_head);

  return 0;
}

static struct identity *identity_find(int id)
{
  struct list_head* pos;
  struct identity* temp;
  list_for_each(pos, &identity_head) {
    temp = list_entry(pos, struct identity, list_node);
    if (temp->id == id){
      pr_info("Found node id: %d name: %s\n", temp->id, temp->name);
      return temp;
    }
  }
  return NULL;
}

static void identity_destroy(int id)
{
  struct list_head* pos, *temp_storage;
  struct identity* cur; //current item;
  list_for_each_safe(pos, temp_storage, &identity_head) {
    cur = list_entry(pos, struct identity, list_node);
    if (cur->id == id) {
      pr_info("Deleting node id: %d name: %s\n", cur->id, cur->name);
      list_del(pos);
      kmem_cache_free(eud_poll, cur);
      break;
    }
  }
}

static struct identity *identity_get(void) 
{
  struct identity *temp;

  if (list_empty(&identity_head))
    return NULL;
  mutex_lock_interruptible(&i_mutex);
  temp = list_entry(identity_head.next, struct identity, list_node);
  list_del(&temp->list_node);
  mutex_unlock(&i_mutex);

  return temp;
}

static ssize_t misc_write(struct file* f, const char __user *buf, size_t count, loff_t* pos)
{
  char input[NAME_LEN];
  int real_count = (count >= NAME_LEN) ? NAME_LEN - 1 : count;

  memset(input, '\0', NAME_LEN);

  if (copy_from_user(input, buf, real_count))
    return -EINVAL;
  if (identity_create(input, counter))
    return -EINVAL;

  counter++;
  wake_up(&wee_wait);
  return count;
}

static struct file_operations misc_fops = {
 .write = misc_write,
 .owner = THIS_MODULE,
};

static struct miscdevice eudyptula_misc = {
  .name = MODULE_NAME,
  .minor = MISC_DYNAMIC_MINOR,
  .fops = &misc_fops,
  .mode = S_IWUSR | S_IWGRP | S_IWOTH,
};

static int eudyptula_fn(void *data) {
  struct identity *temp;

  while (1) {
    if (wait_event_interruptible(wee_wait, 1))
      return -ERESTARTSYS;
    if (kthread_should_stop())
      break;
    temp = identity_get();

    if (temp) {
      msleep_interruptible(5000);
      pr_debug("Got identity: %s %i \n", temp->name, temp->id);
      kfree(temp);
    }
  }
  return 0;
}

int __init hello_init(void)
{
  int ret;
  struct identity *temp;

  eud_poll = kmem_cache_create("eudyptula_cache", sizeof(struct identity), 0, SLAB_POISON, NULL);
  if (IS_ERR(eud_poll)) {
    pr_err("Cannot create cache\n");
    return PTR_ERR(eud_poll);
  }

  pr_info("Hello world!\n");
  ret = misc_register(&eudyptula_misc);
  
  if (ret < 0) {
    pr_err("Misc registration was failed\n");
    return -ENODEV;
  }

  eudyptula_thread = kthread_create(eudyptula_fn, NULL, "eudyptula");
  if (IS_ERR(eudyptula_thread)) {
    misc_deregister(&eudyptula_misc);
    return PTR_ERR(eudyptula_thread);
  }
  wake_up_process(eudyptula_thread);
  
  return 0;
}

void __exit hello_exit(void)
{
  struct list_head* pos, *temp_storage;
  struct identity* cur; //current item;
  list_for_each_safe(pos, temp_storage, &identity_head) {
    cur = list_entry(pos, struct identity, list_node);
    list_del(pos);
    kmem_cache_free(eud_poll, cur);
  }

  kthread_stop(eudyptula_thread);
  misc_deregister(&eudyptula_misc);
  pr_info("Goodbye world!\n");
}

module_init(hello_init);
module_exit(hello_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Phi Nguyen<phind.uet@gmail.com>");
MODULE_DESCRIPTION("Simplest kernel module");
