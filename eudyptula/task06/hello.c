/*
 * Phi Nguyen <phind.uet@gmail.com>
 * This is source code for a simpile kernel module to demonstrate the loading
 * and unloading operation
 */
#include <linux/module.h>
#include <linux/init.h>
#include <linux/miscdevice.h>
#include <linux/uaccess.h>

#define MODULE_NAME "eudyptula"

static ssize_t misc_read(struct file* f, char __user *buf, size_t buf_size, loff_t* pos)
{
  char* reply = "dantri.com";
  int ret;

  if (*pos != 0) 
  {
    pr_err("read overflow!\n");
    ret = -EFAULT;
    goto out;
  }

  if (copy_to_user(buf, reply, buf_size) == 0)
  {
    pr_info("Copy string %s to userspace\n", reply);
    *pos += buf_size,
    ret = buf_size;
  } else {
    pr_err("Cannot copy to user space\n");
    ret = -EFAULT;
  }

out:
  return ret;
}

static ssize_t misc_write(struct file* f, const char __user *buf, size_t buf_size, loff_t* pos)
{
  char kbuf[256];
  int ret;

  if (*pos + buf_size > 255) {
    ret = -EFAULT;
    goto out;
  }

  ret = copy_from_user(kbuf, buf, buf_size);
out:
  return ret;
}

static struct file_operations misc_fops = {
 .read = misc_read,
 .write = misc_write,
 .owner = THIS_MODULE,
};

static struct miscdevice eudyptula_misc = {
  .name = MODULE_NAME,
  .minor = MISC_DYNAMIC_MINOR,
  .fops = &misc_fops,
};

int __init hello_init(void)
{
  int ret;

  pr_info("Hello world!\n");
  ret = misc_register(&eudyptula_misc);
  
  if (ret < 0) {
    pr_err("Misc registration was failed\n");
    return -ENODEV;
  }

  return 0;
}

void __exit hello_exit(void)
{
  misc_deregister(&eudyptula_misc);
  pr_info("Goodbye world!\n");
}

module_init(hello_init);
module_exit(hello_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Phi Nguyen<phind.uet@gmail.com>");
MODULE_DESCRIPTION("Simplest kernel module");
