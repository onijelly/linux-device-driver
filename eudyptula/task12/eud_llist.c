/*
 * Phi Nguyen <phind.uet@gmail.com>
  *   - You have a structure that has 3 fields:
  *        char  name[20];
  *        int   id;
  *        bool  busy;
  *    name this structure "identity".
  *  - Your module has a static variable that points to a list of these
  *    "identity" structures.
  *  - Write a function that looks like:
  *        int identity_create(char *name, int id)
  *    that creates the structure "identity", copies in the 'name' and 'id'
  *    fields and sets 'busy' to false.  Proper error checking for out of
  *    memory issues is required.  Return 0 if everything went ok; an error
  *    value if something went wrong.
  *  - Write a function that looks like:
  *        struct identity *identity_find(int id);
  *    that takes a given id, iterates over the list of all ids, and
  *    returns the proper 'struct identity' associated with it.  If the
  *    identity is not found, return NULL.
  *  - Write a function that looks like:
  *        void identity_destroy(int id);
  *    that given an id, finds the proper 'struct identity' and removes it
  *    from the system.
  *  - Your module_init() function will look much like the following:
  *
  *        struct identity *temp;
  *
  *        identity_create("Alice", 1);
  *        identity_create("Bob", 2);
  *        identity_create("Dave", 3);
  *        identity_create("Gena", 10);
  *
  *        temp = identity_find(3);
  *        pr_debug("id 3 = %s\n", temp->name);
  *
  *        temp = identity_find(42);
  *        if (temp == NULL)
  *                pr_debug("id 42 not found\n");
  *
  *        identity_destroy(2);
  *        identity_destroy(1);
  *        identity_destroy(10);
  *        identity_destroy(42);
  *        identity_destroy(3);
  *
 * This is source code for a simpile kernel module to demonstrate the loading
 * and unloading operation
 */
#include <linux/module.h>
#include <linux/init.h>
#include <linux/list.h>
#include <linux/slab.h>

struct identity 
{
  char name[20];
  int id;
  bool busy;
  struct list_head list_node; 
};

static struct kmem_cache *eud_poll;
LIST_HEAD(identity_head);

static int identity_create(char* name, int id)
{
  struct identity *new_item = kmem_cache_alloc(eud_poll, GFP_KERNEL);
  if (IS_ERR(new_item))
    return PTR_ERR(new_item);
  new_item->id = id;
  memcpy(new_item->name, name, 20 > strlen(name) ? strlen(name) : 20);
  list_add(&new_item->list_node, &identity_head);

  return 0;
}

static struct identity *identity_find(int id)
{
  struct list_head* pos;
  struct identity* temp;
  list_for_each(pos, &identity_head) {
    temp = list_entry(pos, struct identity, list_node);
    if (temp->id == id){
      pr_info("Found node id: %d name: %s\n", temp->id, temp->name);
      return temp;
    }
  }
  return NULL;
}

static void identity_destroy(int id)
{
  struct list_head* pos, *temp_storage;
  struct identity* cur; //current item;
  list_for_each_safe(pos, temp_storage, &identity_head) {
    cur = list_entry(pos, struct identity, list_node);
    if (cur->id == id) {
      pr_info("Deleting node id: %d name: %s\n", cur->id, cur->name);
      list_del(pos);
      kmem_cache_free(eud_poll, cur);
      break;
    }
  }
}

int __init eudyptula_init(void)
{
  struct identity *temp;

  eud_poll = kmem_cache_create("eudyptula_cache", sizeof(struct identity), 0, SLAB_POISON, NULL);
  if (IS_ERR(eud_poll)) {
    pr_err("Cannot create cache\n");
    return PTR_ERR(eud_poll);
  }

  identity_create("Alice", 1);
  identity_create("Bob", 2);
  identity_create("Dave", 3);
  identity_create("Gena", 10);

  temp = identity_find(3);
  pr_debug("id 3 = %s\n", temp->name);

  temp = identity_find(42);
  if (temp == NULL)
          pr_debug("id 42 not found\n");

  identity_destroy(2);
  identity_destroy(1);
  identity_destroy(10);
  identity_destroy(42);
  identity_destroy(3);

  return 0;
}

void __exit eudyptula_exit(void)
{
  if (eud_poll)
    kmem_cache_destroy(eud_poll);
}

module_init(eudyptula_init);
module_exit(eudyptula_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Phi Nguyen<phind.uet@gmail.com>");
MODULE_DESCRIPTION("Simplest kernel module");
