/*
 * Phi Nguyen <phind.uet@gmail.com>
 * This is source code for a simpile kernel module to demonstrate the loading
 * and unloading operation
 */
#include <linux/module.h>
#include <linux/init.h>
#include <linux/netfilter.h>
#include <linux/netfilter_ipv4.h>
#include <linux/ip.h>

static unsigned int eud_hookfn(void *priv, struct sk_buff *skb, const struct nf_hook_state *state)
{
  struct iphdr *iph;

  iph = ip_hdr(skb);

  if (iph->protocol == IPPROTO_TCP)
    dev_info(&state->in->dev, "Received a TCP packet\n");
  else if (iph->protocol == IPPROTO_UDP)
    dev_info(&state->in->dev, "Received an UDP packet\n");

  return NF_ACCEPT;
}

static struct nf_hook_ops eud_nf = {
  .hook = eud_hookfn,
  .hooknum = NF_INET_LOCAL_IN,
  .pf = PF_INET,
  .priority = NF_IP_PRI_FIRST,
};

int __init nethook_init(void)
{
  pr_info("Registering hook\n");
  return nf_register_net_hook(&init_net, &eud_nf);
}

void __exit nethook_exit(void)
{
  pr_info("Unregister hook\n");
  nf_unregister_net_hook(&init_net, &eud_nf);
}

module_init(nethook_init);
module_exit(nethook_exit);

MODULE_LICENSE("GPL");
MODULE_AUTHOR("Phi Nguyen<phind.uet@gmail.com>");
MODULE_DESCRIPTION("Simplest kernel module");
